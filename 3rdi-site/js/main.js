$(function () {
    $('#rotate-btn').on('click', () => {
        $('.top-section__interface').fadeOut();
        $('.interface-view-mode').fadeIn();
    })
    $('.interface-view-mode__btn').on('click', () => {
        $('.top-section__interface').fadeIn();
        $('.interface-view-mode').fadeOut();
    })
});